package lib

type ConfigData struct {
    Environments struct {
        EnvName        string `json:"env_name"`
        AwsProfileName string `json:"aws_profile_name"`
        StageName      string `json:"stage"`
        VpcId          string `json:"id"`
    }
}
