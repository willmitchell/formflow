# formflow: A sample CLI tool for automating CloudFormation workflows

[![pipeline status](https://gitlab.com/willmitchell/formflow/badges/master/pipeline.svg)](https://gitlab.com/willmitchell/formflow/commits/master)
[![coverage report](https://gitlab.com/willmitchell/formflow/badges/master/coverage.svg)](https://gitlab.com/willmitchell/formflow/commits/master)

We can use tools to eliminate the need for manual administrative processes.  This is particularly important when crossing organizational or continental boundaries.  

You might think of this tool as an intentional example of [Infrastructure as Code.](https://www.thoughtworks.com/insights/blog/infrastructure-code-reason-smile)

We already have a similar tool (vfcat) that we use for other cloud automation tasks.  Vfcat is a general 
purpose tool, whereas this tool is intended to be focused on specific projects or customers.  

Currently, this tool has a CLI user interface with no implementation behind it.  The CLI interface is functional, but the implementation is skeletal.  We can build tools like this for internal 
customers and partners in order to help them deliver apps that are aligned with our cloud strategy.

formflow features:

- CLI experience like vfcat (the predecessor of this tool)
- Supports CRUD workflows for CloudFormation (CF) resources
- Support for other ad-hoc administrative tasks
- Statically linked executables available for download for Windows, Linux, OSX (download and run)

Libraries involved:

 - [Cobra](https://github.com/spf13/cobra) # Awesome library for command-oriented CLI's
 - [Viper](https://github.com/spf13/viper) # companion to Cobra that handles environment variables + config files
 - [GoFormation](https://github.com/awslabs/goformation) # AWS Labs libary for **typesafe generation of CF templates**

# Why Golang (aka Go)?

Because the language is solid, testable, and typesafe.  It is fairly easy to learn.

Golang produces statically linked executables that are super easy to deploy.  The value of this is difficult to underestimate for a Cloud Platform team.

Golang is arguably the best language for cloud automation at this point.  Kubernetes and all of the Hashicorp tools are written in Go.

# Getting Started: VF Cloud Platform Customers

This project has a CI-CD pipeline that produces downloadable artifacts associated with each successful build.  
You can download the binary for your platform (Windows, Linux, OSX) and just run it.

[Download](https://gitlab.com/willmitchell/formflow/-/jobs/artifacts/master/browse?job=deploy)

# Getting Started: VF Cloud Platform Developers

- install [Golang](https://golang.org/)
- install (Goland)[https://www.jetbrains.com/go/] or [VS Code](https://code.visualstudio.com/) 
- install [dep](https://github.com/golang/dep)
- install dependencies

```
    dep ensure
```
        
- run the program:

```
    go run main.go
```

Here is what the UI of the tool looks like:

![](./docs/ui.png)

And here is what the CI-CD pipeline looks like:

![](./docs/cicd.png)
